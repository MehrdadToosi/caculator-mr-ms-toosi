package ir.razmkhah.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv1;
    Button btn1;
    Button btn2;
    Button btn3;

    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btn0;

    Button btnPlus;
    Button btnminu;
    Button btnEqual;
    Button btnzarb;
    Button btntag;
    int num1 = 0 ;
    int num3 = 0 ;
    int num4 = 0 ;
    int num5 = 0 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv1 = (TextView) findViewById(R.id.tv1);
        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnPlus = (Button) findViewById(R.id.btnplus);
        btnEqual = (Button) findViewById(R.id.btnequal);
        btnminu=(Button)findViewById(R.id.btnminu);
        btnzarb=(Button)findViewById(R.id.btnzarb);
        btntag=(Button)findViewById(R.id.btntag);
        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnEqual.setOnClickListener(this);
        btnminu.setOnClickListener(this);
        btnzarb.setOnClickListener(this);
        btntag.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case  R.id.btn0:
                show("0");
                break;
            case R.id.btn1:
                show("1");
                break;
            case  R.id.btn2:
                show("2");
                break;
            case  R.id.btn3:
                show("3");
                break;
            case  R.id.btn4:
                show("4");
                break;
            case  R.id.btn5:
                show("5");
                break;
            case  R.id.btn6:
                show("6");
                break;
            case  R.id.btn7:
                show("7");
                break;
            case  R.id.btn8:
                show("8");
                break;
            case  R.id.btn9:
                show("9");
                break;
            case R.id.btnplus:
                plusReady();
                break;
            case R.id.btnequal:
                calculate();
                break;
            case R.id.btnminu:
                minusready();
                break;
            case R.id.btnzarb:
                zarbready();
                break;
            case R.id.btntag:
             tagsimready();
                break;
        }

    }

    private void calculate(){
        int num2 = Integer.parseInt(tv1.getText().toString());
        int resultt = minus(num3 , num2);
        int result = plus(num1 , num2);
        int resulttt =zarb(num4,num2);
        int resultttt=tagsim(num5,num2);
        show(String.valueOf(result) , true);
        show(String.valueOf(resultt),true);
        show(String.valueOf(resulttt),true);
        show(String.valueOf(resultttt),true);

    }
    private int plus(int a , int b){
        return  a + b;
    }
    private void plusReady(){
        num1 = Integer.parseInt(tv1.getText().toString());
        tv1.setText("0");
    }

    private int minus(int q , int w){
        return  q-w;
    }
    private void minusready(){
        num3 = Integer.parseInt(tv1.getText().toString());
        tv1.setText("0");
    }

    private int zarb(int e , int r){
        return  e*r;
    }
    private void zarbready(){
        num4 = Integer.parseInt(tv1.getText().toString());
        tv1.setText("0");
    }

    private int tagsim(int p , int o){
        return  p/o;
    }
    private void tagsimready(){
        num5 = Integer.parseInt(tv1.getText().toString());
        tv1.setText("0");
    }


    private void show(String n , Boolean force) {
        if(force){
            tv1.setText(n);
        }else {
            String current = tv1.getText().toString();
            if (current.equals("0")) {
                tv1.setText(n);}
             else {
                tv1.append(n);
            }
        }
    }
    private void show(String n){
        String current = tv1.getText().toString();
        if (current.equals("0")) {
            tv1.setText(n);
        } else {
            tv1.append(n);
        }
    }
}
